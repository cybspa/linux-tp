# I. Setup base de données

## 1. Install MariaDB

🌞 **Installer MariaDB sur la machine `db.tp2.cesi`**

```bash
#install de maria db

sudo dnf install mariadb-server
```

🌞 **Le service MariaDB**

```bash
[cyb@db ~]$ systemctl start mariadb
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'mariadb.service'.
Multiple identities can be used for authentication:
 1.  cyb
 2.  spa
Choose identity to authenticate as (1-2): 1
Password:
==== AUTHENTICATION COMPLETE ====
[cyb@db ~]$ sudo systemctl enable mariadb
[sudo] password for cyb:
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```

```bash
[cyb@db ~]$ systemctl status mariadb.service
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-14 14:42:33 CET; 4min 28s ago
[cyb@db ~]$ sudo ss -lutpn
Netid   State    Recv-Q   Send-Q      Local Address:Port       Peer Address:Port   Process
tcp     LISTEN   0        128               0.0.0.0:33              0.0.0.0:*       users:(("sshd",pid=873,fd=5))
tcp     LISTEN   0        80                      *:3306                  *:*       users:(("mysqld",pid=3119,fd=21))
tcp     LISTEN   0        128                  [::]:33                 [::]:*       users:(("sshd",pid=873,fd=7))
```

```bash
ps -ef | grep "mysqld"
mysql       3119       1  0 14:42 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr
cyb         3228    2948  0 14:47 pts/0    00:00:00 grep --color=auto mysqld
```

🌞 **Firewall**

```bash
[cyb@db ~]$ sudo firewall-cmd --add-port=3306/tcp
[sudo] password for cyb:
success
[cyb@db ~]$ sudo firewall-cmd --list-all
[...]
 ports: 33/tcp 80/tcp 3306/tcp
```

## 2. Conf MariaDB


🌞 **Configuration élémentaire de la base**

```bash
[cyb@db etc]$ sudo mysql_secure_installation
[...]
All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

---

🌞 **Préparation de la base en vue de l'utilisation par NextCloud**

- pour ça, il faut vous connecter à la base
- il existe un utilisateur `root` dans la base de données, qui a tous les droits sur la base
  - ce n'est pas le même `root` que sur l'OS, c'est celui qui existe dans la base de données
- si vous avez correctement répondu aux questions de `mysql_secure_installation`, vous ne pouvez utiliser le user `root` de la base de données qu'en vous connectant localement à la base
  - c'est à dire, pas à distance, pas depuis le réseau
- donc, sur la VM `db.tp2.cesi` toujours :

```bash
# Connexion à la base de données
[cyb@db etc]$ sudo mysql -u root -p
[sudo] password for cyb:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 24
Server version: 10.3.28-MariaDB MariaDB Server
```

Puis, dans l'invite de commande SQL :

```sql
# Création d'un utilisateur "nextcloud" dans la base, avec un mot de passe
# L'adresse IP correspond à l'adresse IP depuis laquelle viendra les connexions. Cela permet de restreindre les IPs autorisées à se connecter.
# Dans notre cas, c'est l'IP de web.tp2.cesi
# "meow" c'est le mot de passe :D
CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'meow';

# Création de la base de donnée qui sera utilisée par NextCloud
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

# On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';

# Actualisation des privilèges
FLUSH PRIVILEGES;
```

## 3. Test

➜ **On va tester que la base sera utilisable par NextCloud.**

Concrètement il va faire quoi NextCloud vis-à-vis de la base MariaDB ?

- se connecter sur le port où écoute MariaDB
- la connexion viendra de `web.tp2.cesi`
- il se connectera en utilisant l'utilisateur `nextcloud`
- il écrira/lira des données dans la base `nextcloud`

➜ Il faudrait donc qu'on teste ça, à la main, **depuis la machine `web.tp2.cesi`**.

Bah c'est parti ! Il nous faut juste un client pour nous connecter à la base depuis la ligne du commande : il existe une commande `mysql` pour ça.

🌞 **Installez sur la machine `web.tp2.cesi` la commande `mysql`**

- vous utiliserez la commande `dnf provides <COMMAND>` pour trouver dans quel paquet se trouve cette commande

🌞 **Tester la connexion**

```sql
[cyb@web ~]$  mysql -u nextcloud -h 10.2.1.12 -p nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 41
```
```sql
mysql> SHOW TABLES
    ->
```

# II. Setup Apache

## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp2.cesi`**

```bash
sudo dnf install httpd
```

🌞 **Analyse du service Apache**

```bash
[cyb@web ~]$ systemctl start httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'httpd.service'.
Multiple identities can be used for authentication:
 1.  cyb
 2.  spa
Choose identity to authenticate as (1-2): 1
Password:
==== AUTHENTICATION COMPLETE ====
```

```bash
[cyb@web ~]$ sudo systemctl enable httpd
[cyb@web ~]$ sudo ss -lutpn
Netid State  Recv-Q Send-Q Local Address:Port Peer Address:PortProcess
[...]
tcp   LISTEN 0      128                *:80              *:*    users:(("httpd",pid=2670,fd=4),("httpd",pid=2669,fd=4),("httpd",pid=2668,fd=4),("httpd",pid=2666,fd=4))
[cyb@web ~]$ ps -ef | grep "httpd"
root        2666       1  0 16:07 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2667    2666  0 16:07 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2668    2666  0 16:07 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2669    2666  0 16:07 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2670    2666  0 16:07 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
cyb         2930    1756  0 16:08 pts/0    00:00:00 grep --color=auto httpd
```
Port 80

User : apache

🌞 **Un premier test**

```bash
[cyb@web ~]$ sudo firewall-cmd --list-all
[...]
ports: 33/tcp 80/tcp
````
Dans un autre PowerShell :

```bash
PS C:\Users\Samuel> curl 10.2.1.11
curl : <!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
```

### B. PHP

NextCloud a besoin d'une version bien spécifique de PHP.  
Suivez **scrupuleusement** les instructions qui suivent pour l'installer.

🌞 **Installer PHP**

```bash
# ajout des dépôts EPEL
$ sudo dnf install epel-release
$ sudo dnf update
# ajout des dépôts REMI
$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
$ dnf module enable php:remi-7.4

# install de PHP et de toutes les libs PHP requises par NextCloud
$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
```

## 2. Conf Apache

➜ Le fichier de conf principal utilisé par Apache est `/etc/httpd/conf/httpd.conf`.  
Il y en a plein d'autres : ils sont inclus par le fichier de conf principal.

➜ Dans Apache, il existe la notion de *VirtualHost*. On définit des *VirtualHost* dans les fichiers de conf d'Apache.  
On crée un *VirtualHost* pour chaque application web qu'héberge Apache.

> "Application Web" c'est le terme de hipster pour désigner un site web. Disons qu'aujourd'hui les sites peuvent faire tellement de trucs qu'on appelle plutôt ça une "application" plutôt qu'un "site". Une application web donc.

➜ Dans le dossier `/etc/httpd/` se trouve un dossier `conf.d`.  
Des dossiers qui se terminent par `.d`, vous en rencontrerez plein (pas que pour Apache) **ce sont des dossiers de *drop-in*.**

Plutôt que d'écrire 40000 lignes dans un seul fichier de conf, on éclate la configuration dans plusieurs fichiers.  
C'est + lisible et + facilement maintenable.

**Les dossiers de *drop-in* servent à accueillir ces fichiers de conf additionels.**  
Le fichier de conf principal a une ligne qui inclut tous les autres fichiers de conf contenus dans le dossier de *drop-in*.

---

🌞 **Analyser la conf Apache**

````bash
[cyb@web ~]$ cat /etc/httpd/conf/httpd.conf | grep conf.d
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
````

🌞 **Créer un VirtualHost qui accueillera NextCloud**



```bash
sudo nano cesi.tp2.web.conf
sudo systemctl restart httpd
```


🌞 **Configurer la racine web**

```bash
[cyb@web conf]$ sudo mkdir /var/www/nextcloud/html/ -p
[sudo] password for cyb:
[cyb@web conf]$ cd /
[cyb@web /]$ cd /var/www/nextcloud/html/
[cyb@web html]$ cd ..
```
````bash
[cyb@web nextcloud]$ sudo chown -R apache html
[cyb@web nextcloud]$ ls -al
total 0
drwxr-xr-x. 3 root root  18 Dec 14 22:51 .
drwxr-xr-x. 5 root root  50 Dec 14 22:51 ..
drwxr-xr-x. 2 apache apache  6 Dec 14 22:51 html
[cyb@web nextcloud]$ cd html
[cyb@web html]$ mkdir testD
[cyb@web html]$ cd testD/
[cyb@web testD]$ touch test.txt
[cyb@web testD]$ ls -al
drwxr-xr-x.  4 apache apache   36 Dec 14 23:50 .
drwxr-xr-x.  3 root   root     18 Dec 14 22:51 ..
drwxrwxr-x.  2 apache apache   22 Dec 14 23:21 testD
````

🌞 **Configurer PHP**

```bash
[cyb@web html]$ timedatectl
               Local time: Tue 2021-12-14 23:27:43 CET
           Universal time: Tue 2021-12-14 22:27:43 UTC
                 RTC time: Tue 2021-12-14 22:27:42
                Time zone: Europe/Paris (CET, +0100)
```

```bash
[cyb@web html]$ sudo cat /etc/opt/remi/php74/php.ini | grep date.timezone -n
922:; http://php.net/date.timezone
923:;date.timezone =
[cyb@web html]$ sudo nano +915 /etc/opt/remi/php74/php.ini
```

# III. NextCloud

On dit "installer NextCloud" mais en fait c'est juste récupérer les fichiers PHP, HTML, JS, etc... qui constituent NextCloud, et les mettre dans le dossier de la racine web.

🌞 **Récupérer Nextcloud**

```bash
[cyb@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
[cyb@web ~]$ ls
nextcloud-21.0.1.zip
```

🌞 **Ranger la chambre**

```bash
[cyb@web ~]$ unzip nextcloud-21.0.1.zip -d /var/www/nextcloud/html/
[cyb@web ~]$ cd /var/www/nextcloud/html/
[cyb@web html]$ ls
nextcloud  testD
[cyb@web nextcloud]$ sudo chown -R apache ./*
[sudo] password for cyb:
[cyb@web nextcloud]$ sudo chown -R apache ./.user.ini
[cyb@web nextcloud]$ sudo chown -R apache ./.htaccess
[cyb@web nextcloud]$ ls -al
-rw-r--r--.  1 apache apache 34520 Apr  8  2021 COPYING
drwxr-xr-x. 22 apache apache  4096 Apr  8  2021 core
-rw-r--r--.  1 apache apache  5122 Apr  8  2021 cron.php
-rw-r--r--.  1 apache apache  2734 Apr  8  2021 .htaccess
-rw-r--r--.  1 apache apache   156 Apr  8  2021 index.html
[...]
[cyb@web nextcloud]$ sudo find / -name nextcloud-21.0.1.zip
/home/cyb/nextcloud-21.0.1.zip
[cyb@web nextcloud]$ rm /home/cyb/nextcloud-21.0.1.zip
[cyb@web nextcloud]$ ls /home/cyb/
[cyb@web nextcloud]$
```


## 4. Test

Bah on arrive sur la fin du setup !

Si on résume :

- **un serveur de base de données : `db.tp2.cesi`**
  - MariaDB installé et fonctionnel
  - firewall configuré
  - une base de données et un user pour NextCloud ont été créés dans MariaDB
- **un serveur Web : `web.tp2.cesi`**
  - Apache installé et fonctionnel
  - firewall configuré
  - un VirtualHost qui pointe vers la racine `/var/www/nextcloud/html/`
  - NextCloud installé dans le dossier `/var/www/nextcloud/html/`

**Looks like we're ready.**

---

**Ouuu presque. Pour que NextCloud fonctionne correctement, il est préférable d'y accéder en utilisant un nom, et pas une IP.**  
On va donc devoir faire en sorte que, depuis votre PC, vous puissiez écrire `http://web.tp2.cesi` plutôt que `http://10.2.1.11`.

➜ Pour faire ça, on va utiliser **le fichier `hosts`**. C'est un fichier présents sur toutes les machines, sur tous les OS.  
Il sert à définir, localement, une correspondance entre une IP et un ou plusieurs noms.  

Emplacement du fichier `hosts` :

- MacOS/Linux : `/etc/hosts`
- Windows : `c:\windows\system32\drivers\etc\hosts`

---

🌞 **Modifiez le fichier `hosts` de votre PC**

```
10.2.1.11 web.tp2.cesi
127.0.0.1 localhost
::1 localhost
```

🌞 **Tester l'accès à NextCloud et finaliser son install'**

````bash
[cyb@web httpd]$ curl http://web.tp2.cesi -L
<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="en" data-locale="en" >
        <head>
````

**🔥🔥🔥 Baboom ! Un beau NextCloud.**

Naviguez un peu, faites vous plais', vous avez votre propre DropBox.

> Ptet le moment de prendre une pause pour laisser votre cerveau respirer ? :)

**Et après, go next : [Partie II : Sécurisation](./part2.md)**